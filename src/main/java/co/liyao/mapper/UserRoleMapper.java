package co.liyao.mapper;


import co.liyao.model.UserRole;
import co.liyao.util.MyMapper;
import org.apache.ibatis.annotations.Param;

public interface UserRoleMapper extends MyMapper<UserRole> {

    public void  delRoleByUserId(@Param("id") Integer id);

}