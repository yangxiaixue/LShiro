package co.liyao.mapper;


import co.liyao.model.User;
import co.liyao.util.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface UserMapper extends MyMapper<User> {

    /**
     * 根据用户名查询用户
     * @param username
     * @return user
     */
    User selectByUsername(String username);


    /**
     * 根据user参数查询用户列表
     * @param user
     * @return list
     */
    List<User> selectUsers(User user);

    /**
     *  根据用户id  返回所有的角色id
     * @param id
     * @return
     */
    List<Integer> selectRoleIdByUserId(@Param("id") Integer id);

    /**
     *  根据决策id 返回所有的用户
     * @param id
     * @return
     */
    List<User> findByRoleId(@Param("id") Integer id);


    /**
     * 根据用户id更新用户信息
     * @param user
     * @return int
     */
    int updateByUserId(User user);


    /**
     * 根据用户ID查询用户
     * @param userId
     * @return user
     */
    User selectByUserId(String userId);
}
