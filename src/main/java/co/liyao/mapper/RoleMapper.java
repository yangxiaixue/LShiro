package co.liyao.mapper;

import co.liyao.model.Role;
import co.liyao.util.MyMapper;

import java.util.Map;
import java.util.Set;

public interface RoleMapper extends MyMapper<Role> {

    /**
     * 根据用户id查询角色集合
     * @param userId 用户id
     * @return set
     */
    Set<String> findRoleByUserId(String userId);

    /**
     * 根据roleId更新角色信息
     * @param params
     * @return int
     */
    int updateByRoleId(Map<String, Object> params);

}