package co.liyao.mapper;


import co.liyao.model.Permission;
import co.liyao.util.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface PermissionMapper extends MyMapper<Permission> {

    /**
     * 根据用户id查询权限集合
     * @param userId 状态
     * @return set
     */
    Set<String> findPermsByUserId(String userId);


    /**
     * 根据用户id查询菜单
     * @param userId 用户id
     * @return the list
     */
    List<Permission> selectMenuByUserId(String userId);

    /**
     * 根据状态查询全部资源
     * @param status 状态
     * @return the list
     */

    List<Permission> selectAllPerms(Integer status);

    /**
     *  根据当前资源资源ID统计下机资源的条数
     * @param id
     * @return
     */
    Integer selectCountParentIdbyPermissionId(@Param("id") Integer id);

    /**
     *  查询资源菜单and 目录
     * @return
     */
    List<Permission> findNotButByAll();


    /**
     * 根据权限id查询权限实体
     * @param permissionId 权限id
     * @return permission
     */
    Permission selectByPermissionId(String permissionId);

    /**
     * 根据权限bean修改权限
     * @param permission 权限
     * @return int
     */
    int updateByPermissionId(Permission permission);
}