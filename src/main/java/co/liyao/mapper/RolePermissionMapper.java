package co.liyao.mapper;


import co.liyao.model.RolePermission;
import co.liyao.util.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RolePermissionMapper extends MyMapper<RolePermission> {

    List<String> findPermissionIdAllByRoleId(@Param("id") String id);

     void delPermissionByRoleId(@Param("id") Integer id);

}