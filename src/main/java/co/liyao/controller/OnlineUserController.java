package co.liyao.controller;

import co.liyao.dto.UserOnlineVo;
import co.liyao.service.UserService;
import co.liyao.util.JsonData;
import co.liyao.util.PageResultVo;
import com.google.common.collect.Lists;
import org.apache.commons.collections.ListUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: liyao
 * @Date: 2019/2/24 21:34
 * @Description:
 */
@Controller
@RequestMapping("/online/user")
public class OnlineUserController {

    @Autowired
    private UserService userService;

    // 在线用户列表
    @GetMapping("/list")
    @ResponseBody
    public PageResultVo onlineUsers(UserOnlineVo user, Integer limit, Integer page){
        List<UserOnlineVo> userList = userService.onlineVoList(user);

        // 根据以上进行内存分页（原始数据，显示的条数） guava的工具类
        List<List<UserOnlineVo>> partition = Lists.partition(userList, 10);
        // 根据页码 显示当前页的数据 因为下标从 0 开始
        List<UserOnlineVo> userOnlineVos = partition.get(page - 1 );
        return JsonData.table(userOnlineVos,(long)userList.size());
    }

    // 强制踢出用户
    @PostMapping("/kickout")
    @ResponseBody
    public JsonData kickout(String sessionId,String username) {
        try {
//            if(SecurityUtils.getSubject().getSession().getId().equals(sessionId)){
//                return ResultUtil.error("不能踢出自己");
//            }
            userService.kickout(sessionId,username);
            return JsonData.success(null,"踢出用户成功");
        } catch (Exception e) {
            return JsonData.fail("踢出用户失败");
        }
    }



}
