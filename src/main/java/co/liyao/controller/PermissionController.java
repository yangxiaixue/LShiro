package co.liyao.controller;

import co.liyao.model.Permission;
import co.liyao.service.PermissionService;
import co.liyao.shiro.service.ShiroService;
import co.liyao.util.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Auther: liyao
 * @Date: 2019/2/25 13:22
 * @Description:
 */
@Controller
public class PermissionController  {

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private ShiroService shiroService;


    @GetMapping("/selectPermissionAll")
    @ResponseBody
    public JsonData permission(){
        List<Permission> permissions = permissionService.selectAll(1);
        return JsonData.success(permissions);
    }

    @GetMapping("/SelectCountPermissionId")
    @ResponseBody
    public JsonData countId(Integer id){
        return JsonData.success(permissionService.selectcountId(id),"请求成功");
    }



    @GetMapping("/permission/del")
    @ResponseBody
    public JsonData delId(Integer id){
        try{
            permissionService.delId(id);
        }catch (Exception ex){
            JsonData.fail("出现错误");
        }
        return JsonData.success(null,"请求成功");
    }


    /*添加权限*/
    @ResponseBody
    @PostMapping("/permission/add")
    public JsonData addPermission(Permission permission){
        try {
            int a = permissionService.insert(permission);
            if (a > 0) {
                shiroService.updatePermission();
                return JsonData.success("添加权限成功");
            } else {
                return JsonData.fail("添加权限失败");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @GetMapping("/permission/findNotButByAll")
    @ResponseBody
    public JsonData findNotButByAll(){
        return JsonData.success(permissionService.findNotButByAll(),"查询成功!");
    }


    @GetMapping("/permission/edit")
    public String detail(Model model, String permissionId) {
        Permission permission = permissionService.findByPermissionId(permissionId);
        if(null!=permission){
            if(permission.getParentId().equals(0)){
                model.addAttribute("parentName", "顶层菜单");
            }else{
                Permission parent = permissionService.findById(permission.getParentId());
                model.addAttribute("parentName", parent.getName());
            }
        }
        model.addAttribute("permission", permission);
        return "permissions/detail";
    }



    /*编辑权限*/
    @ResponseBody
    @PostMapping("/permission/edit")
    public JsonData editPermission(@ModelAttribute("permission")Permission permission){
        int a = permissionService.updateByPermissionId(permission);
        if (a > 0) {
            shiroService.updatePermission();
            return JsonData.success("编辑权限成功");
        } else {
            return JsonData.fail("编辑权限失败");
        }
    }

}


