package co.liyao.controller;

import co.liyao.dto.PermissionsDto;
import co.liyao.model.Role;
import co.liyao.model.User;
import co.liyao.service.RoleService;
import co.liyao.shiro.realm.UserRealm;
import co.liyao.util.JsonData;
import co.liyao.util.PageResultVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: liyao
 * @Date: 2019/2/23 09:37
 * @Description:
 */
@RequestMapping("/role")
@Controller
public class RoleController  {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRealm userRealm;



    /*新增角色*/
    @PostMapping("/add")
    @ResponseBody
    public JsonData addRole(Role role) {
        try {
            int a = roleService.insert(role);
            if (a > 0) {
                return JsonData.success(null,"添加角色成功");
            } else {
                return JsonData.fail("添加角色失败");
            }
        } catch (Exception e) {
            throw e;
        }
    }


    @GetMapping("/list")
    @ResponseBody
    public PageResultVo loadRole(String rolename,Integer limit, Integer page){
        PageHelper.startPage(page,limit);
        List<Role> roleList = roleService.findRoleAllByPage(rolename);
        PageInfo<Role> pages = new PageInfo<>(roleList);
        return JsonData.table(roleList,pages.getTotal());
    }


    @GetMapping("/permissionlistByroleId")
    @ResponseBody
    public JsonData permissionlistByroleId(String roleId){
        List<PermissionsDto> roleAllBySelected = roleService.findRoleAllBySelected(roleId);
        return JsonData.success(roleAllBySelected);
    }

    @PostMapping("/updateRoleIdbyPermissionId")
    @ResponseBody
    public JsonData roleList(final Integer roleid, final Long[] permissionid){
        roleService.updateRoleIdbyPermissionId(roleid,permissionid);
        List<User> userList = roleService.findByRoleId(roleid);
        if(userList.size()>0){
            List<String> userIds = new ArrayList<>();
            for(User user : userList){
                userIds.add(user.getUserId());
            }
            userRealm.clearAuthorizationByUserId(userIds);
        }
        return JsonData.success();
    }



    /*编辑角色详情*/
    @GetMapping("/edit")
    public String detail(Model model, Integer id) {
        Role role = roleService.findById(id);
        model.addAttribute("role", role);
        return "role/RoleDetail";
    }


    @PostMapping("/edit")
    @ResponseBody
    public JsonData editRole(@ModelAttribute("role") Role role) {

        if (role.getStatus() == null){
            role.setStatus(2); //表示不可用状态
        }
        int a = roleService.updateByRoleId(role);
        if (a > 0) {
            return JsonData.success(null,"编辑角色成功");
        } else {
            return JsonData.fail("编辑角色失败");
        }
    }

    /**编辑用户*/
    @PostMapping("/del")
    @ResponseBody
    public JsonData delUser(String roleId){
        try{
            roleService.del(Integer.parseInt(roleId));
        }catch(Exception ex){
            JsonData.fail("出现错误");
        }
        return JsonData.success(null,"删除成功");
    }

}
