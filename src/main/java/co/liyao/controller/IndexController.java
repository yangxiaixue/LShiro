package co.liyao.controller;


import co.liyao.model.Permission;
import co.liyao.model.User;
import co.liyao.service.PermissionService;
import co.liyao.util.JsonData;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * @Auther: liyao
 * @Date: 2019/1/27 21:04

 * @Description:
 */

@Controller
public class IndexController {

    @Autowired
    PermissionService permissionService;

    @RequestMapping("/")
    @ResponseBody
    public String test(){
        return "Hello SpringBoot and Shiro Word21!";
    }

    @PostMapping("/login")
    @ResponseBody
    public JsonData login(String username, String password,
                        @RequestParam(name="rememberme",defaultValue = "false") Boolean rememberme){
        UsernamePasswordToken token=new UsernamePasswordToken(username,password,rememberme);

        try {
            Subject subject= SecurityUtils.getSubject();
            //登录验证
            subject.login(token);
        }catch (Exception e){
            token.clear();
            return JsonData.fail(e.getMessage());
        }
        return JsonData.success("登录成功！");
    }



    /*获取当前登录用户的菜单*/
    @GetMapping("/menu")
    @ResponseBody
    public List<Permission> getMenus(){
        Object principal = SecurityUtils.getSubject().getPrincipal();
        User user = new User();
        BeanUtils.copyProperties(principal,user);
        List<Permission> permissionListList = permissionService.selectMenuByUserId(user.getUserId());
        return permissionListList;
    }

    @GetMapping("/login")
    public String login(){
        Subject subject = SecurityUtils.getSubject();
        // 如果已登录重定向到新页面
        if(subject.isAuthenticated()){
            return "redirect:/index";
        }
        return "login";
    }

    @GetMapping("/index")
    public String index(){
        return "index";
    }

    @GetMapping("/workdest")
    public String workdestiframe(){
        return "index/workdest";
    }

    @GetMapping("/users")
    public String userIframe(){
        return "user/users";
    }

    @GetMapping("/roles")
    public String roleIframe(){
        return "role/roles";
    }

    @GetMapping("/online/users")
    public String onlineframe(){
        return "session/sessions";
    }

    @GetMapping("/permissions")
    public String permissionsframe(){
        return "permissions/permissions";
    }

    /*踢出*/
    @GetMapping("/kickout")
    public String kickout(Map map){
        return "kickout";
    }

    @GetMapping(value = "/database/monitoring")
    public ModelAndView databaseMonitoring(){
        return new ModelAndView("database/monitoring");
    }
}
