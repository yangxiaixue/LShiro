package co.liyao.service;

import co.liyao.dto.PermissionsDto;
import co.liyao.mapper.*;
import co.liyao.model.*;
import co.liyao.util.UUIDUtil;
import com.fasterxml.jackson.databind.util.BeanUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Auther: liyao
 * @Date: 2019/1/29 14:30
 * @Description:
 */
@Service
public class RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private UserMapper userMapper;


    @Autowired
    private UserRoleMapper userRoleMapper;


    public Set<String> findRoleByUserId(String userId) {
        return roleMapper.findRoleByUserId(userId);
    }

    public List<Role> findRoleAllByPage(String name){
        Role role = new Role();
        role.setName(name);
        return roleMapper.select(role);
    }

    public  List<PermissionsDto> findRoleAllBySelected(String roleid){
        List<Permission> permissions = permissionMapper.selectAll(); //菜单的全部数据
        List<String> permissionIdAllByRoleId = rolePermissionMapper.findPermissionIdAllByRoleId(roleid);
        List<PermissionsDto> dto = new LinkedList<>();  // 根据角色区分出已经拥有和未拥有的权限
        for (Permission permission: permissions) {
            if(permissionIdAllByRoleId.contains(permission.getPermissionId())){
                PermissionsDto  permissionsDto = new PermissionsDto();
                BeanUtils.copyProperties(permission,permissionsDto);
                permissionsDto.setChecked(true);
                dto.add(permissionsDto);
            }else{
                PermissionsDto  permissionsDto = new PermissionsDto();
                BeanUtils.copyProperties(permission,permissionsDto);
                permissionsDto.setChecked(false);
                dto.add(permissionsDto);
            }
        }
//        List<PermissionsDto> firstdto = new LinkedList<>();
//
//        for (PermissionsDto dto1 :dto){
//            if(dto1.getParentId() == 0){
//                firstdto.add(dto1);
//            }
//        }
//        for (PermissionsDto dto2 : firstdto) {
//            dto2.setChildren(getChild(dto2.getId(),dto));
//        }
        return dto;
    }

//    public List<PermissionsDto> getChild(Integer id,List<PermissionsDto> listall){
//
//        List<PermissionsDto> permissionsDtos = new ArrayList<>();
//
//        for (PermissionsDto permiss: listall) {
//            if(permiss.getParentId().equals(id)){
//                permissionsDtos.add(permiss);
//            }
//        }
//
//        for(PermissionsDto dto:permissionsDtos){
//            dto.setChildren(getChild(dto.getId(),listall));
//        }
//
//        if(permissionsDtos.size() == 0){
//            return new ArrayList<>();
//        }
//
//        return permissionsDtos;
//    }


    @Transactional
    public void updateRoleIdbyPermissionId(Integer roleId,Long[] permission){
        rolePermissionMapper.delPermissionByRoleId(roleId);
        for (Long lo:permission) {
            RolePermission ur = new RolePermission();
            ur.setPermissionId(String.valueOf(lo));
            ur.setRoleId(String.valueOf(roleId));
            rolePermissionMapper.insert(ur);
        }
    }

    public List<User> findByRoleId(Integer id){
        return userMapper.findByRoleId(id);
    }

    public Role findById(Integer id) {
        Role role = new Role();
        role.setId(id);
        return roleMapper.selectByPrimaryKey(role);
    }

    public int updateByRoleId(Role role) {
        Map<String,Object> params  = new HashMap<>(3);
        params.put("name",role.getName());
        params.put("description",role.getDescription());
        params.put("status",role.getStatus());
        params.put("role_id",role.getRoleId());
        return roleMapper.updateByRoleId(params);
    }

    @Transactional
    public void del(Integer roleId) {
        UserRole role = new UserRole();
        role.setRoleId(String.valueOf(roleId));
        userRoleMapper.delete(role); //删除完全部权限
        roleMapper.deleteByPrimaryKey(roleId);
    }

    public int insert(Role role) {
        role.setRoleId(UUIDUtil.getUniqueIdByUUId());
        role.setStatus(1);
        role.setCreateTime(new Date());
        return roleMapper.insert(role);
    }

}