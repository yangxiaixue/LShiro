package co.liyao.service;

import co.liyao.mapper.PermissionMapper;
import co.liyao.mapper.RolePermissionMapper;
import co.liyao.model.Permission;
import co.liyao.model.RolePermission;
import co.liyao.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Auther: liyao
 * @Date: 2019/1/29 14:48
 * @Description:
 */

@Service
public class PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    public Set<String> findPermsByUserId(String userId) {
        return permissionMapper.findPermsByUserId(userId);
    }

    public List<Permission> selectMenuByUserId(String userId) {

        List<Permission> permissionsAll = permissionMapper.selectMenuByUserId(userId);

        List<Permission> firstData = new ArrayList<>();

        for (Permission permiss : permissionsAll){
            if(permiss.getParentId() == 0){
                firstData.add(permiss);
            }
        }

        for (Permission info : firstData) {
            info.setChildren(getChild(info.getId(),permissionsAll));
        }

        return firstData;
    }
    private List<Permission> getChild(Integer id, List<Permission> morg) {

        List<Permission> child = new ArrayList<>();

        for (Permission per : morg){
            if(per.getParentId().equals(id)){
                child.add(per);
            }
        }
        for(Permission permission : child){
            permission.setChildren(getChild(permission.getId(),morg));
        }

        if(child.size() == 0){
            return null;
        }
        return  child;
    }

    /**
     * 查询全部权限
     * @param status
     * @return list
     */
    public List<Permission> selectAll(Integer status){
        return permissionMapper.selectAllPerms(status);
    };

    /**
     *  根据菜单id 查询下面是否还有
     * @param id
     * @return
     */
    public Integer selectcountId(Integer id){
       return permissionMapper.selectCountParentIdbyPermissionId(id);
    }

    /**
     * 根据资源id 删除资源 并且删除与角色关联的ID
     * @param id
     */
    @Transactional
    public void delId(Integer id) {
        RolePermission rolePermission = new RolePermission();
        rolePermission.setPermissionId(String.valueOf(id));
        rolePermissionMapper.delete(rolePermission);
        permissionMapper.deleteByPrimaryKey(id);
    }

    /**
     *  添加资源信息
     * @param permission
     * @return
     */
    public int insert(Permission permission) {
        Date date = new Date();
        permission.setPermissionId(UUIDUtil.getUniqueIdByUUId());
        permission.setStatus(1);
        permission.setCreateTime(date);
        permission.setUpdateTime(date);
        return permissionMapper.insert(permission);
    }

    /**
     *  查询资源（菜单,目录）
     */
    public List<Permission> findNotButByAll(){
        return permissionMapper.findNotButByAll();
    }

    public Permission findByPermissionId(String permissionId) {
        return permissionMapper.selectByPermissionId(permissionId);
    }

    public Permission findById(Integer id) {
        Permission permission = new Permission();
        permission.setId(id);
        return permissionMapper.selectByPrimaryKey(permission);
    }

    public int updateByPermissionId(Permission permission) {
        return permissionMapper.updateByPermissionId(permission);
    }

}
