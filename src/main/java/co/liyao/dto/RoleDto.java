package co.liyao.dto;

import co.liyao.model.Role;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Auther: liyao
 * @Date: 2019/2/22 10:14
 * @Description:
 */
public class RoleDto{
    @JsonProperty(value = "LAY_CHECKED")
    private boolean selected;

    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
