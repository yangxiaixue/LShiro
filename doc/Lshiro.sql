/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50641
 Source Host           : localhost:3306
 Source Schema         : zb-shiro

 Target Server Type    : MySQL
 Target Server Version : 50641
 File Encoding         : 65001

 Date: 03/03/2019 16:14:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限访问路径',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父级权限id',
  `type` int(1) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `order_num` int(3) NULL DEFAULT 0 COMMENT '排序',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (2, '2', '权限管理', '权限管理', '', NULL, 0, 0, 2, 'fa fa-th-list', 1, '2017-07-13 15:04:42', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (3, '201', '用户管理', '用户管理', '/users', 'users', 2, 1, 1, 'fa fa-circle-o', 1, '2017-07-13 15:05:47', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (4, '20101', '列表查询', '用户列表查询', '/user/list', 'user:list', 3, 2, 0, NULL, 1, '2017-07-13 15:09:24', '2017-10-09 05:38:29');
INSERT INTO `permission` VALUES (5, '20102', '新增', '新增用户', '/user/add', 'user:add', 3, 2, 0, NULL, 1, '2017-07-13 15:06:50', '2018-02-28 17:58:46');
INSERT INTO `permission` VALUES (6, '20103', '编辑', '编辑用户', '/user/edit', 'user:edit', 3, 2, 0, NULL, 1, '2017-07-13 15:08:03', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (7, '20104', '删除', '删除用户', '/user/delete', 'user:delete', 3, 2, 0, NULL, 1, '2017-07-13 15:08:42', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (9, '20106', '分配角色', '分配角色', '/user/assign/role', 'user:assignRole', 3, 2, 0, NULL, 1, '2017-07-13 15:09:24', '2017-10-09 05:38:29');
INSERT INTO `permission` VALUES (10, '202', '角色管理', '角色管理', '/roles', 'roles', 2, 1, 2, 'fa fa-circle-o', 1, '2017-07-17 14:39:09', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (11, '20201', '列表查询', '角色列表查询', '/role/list', 'role:list', 10, 2, 0, NULL, 1, '2017-10-10 15:31:36', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (12, '20202', '新增', '新增角色', '/role/add', 'role:add', 10, 2, 0, NULL, 1, '2017-07-17 14:39:46', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (13, '20203', '编辑', '编辑角色', '/role/edit', 'role:edit', 10, 2, 0, NULL, 1, '2017-07-17 14:40:15', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (14, '20204', '删除', '删除角色', '/role/delete', 'role:delete', 10, 2, 0, NULL, 1, '2017-07-17 14:40:57', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (15, '20205', '批量删除', '批量删除角色', '/role/batch/delete', 'role:batchDelete', 10, 2, 0, '', 1, '2018-07-10 22:20:43', '2018-07-10 22:20:43');
INSERT INTO `permission` VALUES (16, '20206', '分配权限', '分配权限', '/role/assign/permission', 'role:assignPerms', 10, 2, 0, NULL, 1, '2017-09-26 07:33:05', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (17, '203', '资源管理', '资源管理', '/permissions', 'permissions', 2, 1, 3, 'fa fa-circle-o', 1, '2017-09-26 07:33:51', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (18, '20301', '列表查询', '资源列表', '/permission/list', 'permission:list', 17, 2, 0, NULL, 1, '2018-07-12 16:25:28', '2018-07-12 16:25:33');
INSERT INTO `permission` VALUES (19, '20302', '新增', '新增资源', '/permission/add', 'permission:add', 17, 2, 0, NULL, 1, '2017-09-26 08:06:58', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (20, '20303', '编辑', '编辑资源', '/permission/edit', 'permission:edit', 17, 2, 0, NULL, 1, '2017-09-27 21:29:04', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (21, '20304', '删除', '删除资源', '/permission/delete', 'permission:delete', 17, 2, 0, NULL, 1, '2017-09-27 21:29:50', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES (22, '3', '运维管理', '运维管理', '', NULL, 0, 0, 3, 'fa fa-th-list', 1, '2018-07-06 15:19:26', '2018-07-06 15:19:26');
INSERT INTO `permission` VALUES (23, '301', '数据监控', '数据监控', '/database/monitoring', 'database', 22, 1, 1, 'fa fa-circle-o', 1, '2018-07-06 15:19:55', '2019-03-03 15:57:24');
INSERT INTO `permission` VALUES (24, '4', '系统工具', '系统工具', '', NULL, 0, 0, 4, 'fa fa-th-list', 1, '2018-07-06 15:20:38', '2018-07-06 15:20:38');
INSERT INTO `permission` VALUES (25, '401', '图标工具', '图标工具', '/icons', 'icons', 24, 1, 1, 'fa fa-circle-o', 1, '2018-07-06 15:21:00', '2018-07-06 15:21:00');
INSERT INTO `permission` VALUES (28, '1000000884924014', '在线用户', '在线用户', '/online/users', 'onlineUsers', 2, 1, 4, 'fa fa-circle-o', 1, '2018-07-18 21:00:38', '2018-07-19 12:47:42');
INSERT INTO `permission` VALUES (29, '1000000433323073', '在线用户查询', '在线用户查询', '/online/user/list', 'onlineUser:list', 28, 2, 0, NULL, 1, '2018-07-18 21:01:25', '2018-07-19 12:48:04');
INSERT INTO `permission` VALUES (30, '1000000903407910', '踢出用户', '踢出用户', '/online/user/kickout', 'onlineUser:kickout', 28, 2, 0, NULL, 1, '2018-07-18 21:41:54', '2018-07-19 12:48:25');
INSERT INTO `permission` VALUES (31, '1000000851815490', '批量踢出', '批量踢出', '/online/user/batch/kickout', 'onlineUser:batchKickout', 28, 2, 0, '', 1, '2018-07-19 12:49:30', '2018-07-19 12:49:30');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '1', '超级管理员', '超级管理员213', 1, '2017-06-28 20:30:05', '2019-02-27 13:53:48');
INSERT INTO `role` VALUES (2, '2', '管理员', '管理员1', 1, '2017-06-30 23:35:19', '2019-03-02 22:52:44');
INSERT INTO `role` VALUES (8, '1000000808402724', '李耀', '123123', 2, '2019-03-02 22:56:20', '2019-03-02 23:35:41');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `permission_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2558 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES (2402, '2', '2');
INSERT INTO `role_permission` VALUES (2403, '2', '201');
INSERT INTO `role_permission` VALUES (2404, '2', '20101');
INSERT INTO `role_permission` VALUES (2405, '2', '20102');
INSERT INTO `role_permission` VALUES (2406, '2', '20103');
INSERT INTO `role_permission` VALUES (2407, '2', '20104');
INSERT INTO `role_permission` VALUES (2408, '2', '20106');
INSERT INTO `role_permission` VALUES (2409, '2', '202');
INSERT INTO `role_permission` VALUES (2410, '2', '20201');
INSERT INTO `role_permission` VALUES (2411, '2', '20202');
INSERT INTO `role_permission` VALUES (2412, '2', '20203');
INSERT INTO `role_permission` VALUES (2413, '2', '20204');
INSERT INTO `role_permission` VALUES (2414, '2', '20205');
INSERT INTO `role_permission` VALUES (2415, '2', '20206');
INSERT INTO `role_permission` VALUES (2416, '2', '203');
INSERT INTO `role_permission` VALUES (2417, '2', '20301');
INSERT INTO `role_permission` VALUES (2418, '2', '20302');
INSERT INTO `role_permission` VALUES (2419, '2', '20303');
INSERT INTO `role_permission` VALUES (2420, '2', '20304');
INSERT INTO `role_permission` VALUES (2421, '2', '1000000884924014');
INSERT INTO `role_permission` VALUES (2422, '2', '1000000433323073');
INSERT INTO `role_permission` VALUES (2423, '2', '1000000903407910');
INSERT INTO `role_permission` VALUES (2424, '2', '1000000851815490');
INSERT INTO `role_permission` VALUES (2425, '2', '3');
INSERT INTO `role_permission` VALUES (2426, '2', '301');
INSERT INTO `role_permission` VALUES (2427, '2', '4');
INSERT INTO `role_permission` VALUES (2428, '2', '401');
INSERT INTO `role_permission` VALUES (2429, '2', '1000000189866454');
INSERT INTO `role_permission` VALUES (2430, '2', '1000000082934989');
INSERT INTO `role_permission` VALUES (2460, '1', '2');
INSERT INTO `role_permission` VALUES (2461, '1', '201');
INSERT INTO `role_permission` VALUES (2462, '1', '20101');
INSERT INTO `role_permission` VALUES (2463, '1', '20102');
INSERT INTO `role_permission` VALUES (2464, '1', '20103');
INSERT INTO `role_permission` VALUES (2465, '1', '20104');
INSERT INTO `role_permission` VALUES (2466, '1', '20106');
INSERT INTO `role_permission` VALUES (2467, '1', '202');
INSERT INTO `role_permission` VALUES (2468, '1', '20201');
INSERT INTO `role_permission` VALUES (2469, '1', '20202');
INSERT INTO `role_permission` VALUES (2470, '1', '20203');
INSERT INTO `role_permission` VALUES (2471, '1', '20204');
INSERT INTO `role_permission` VALUES (2472, '1', '20205');
INSERT INTO `role_permission` VALUES (2473, '1', '20206');
INSERT INTO `role_permission` VALUES (2474, '1', '203');
INSERT INTO `role_permission` VALUES (2475, '1', '20301');
INSERT INTO `role_permission` VALUES (2476, '1', '20302');
INSERT INTO `role_permission` VALUES (2477, '1', '20303');
INSERT INTO `role_permission` VALUES (2478, '1', '20304');
INSERT INTO `role_permission` VALUES (2479, '1', '1000000884924014');
INSERT INTO `role_permission` VALUES (2480, '1', '1000000433323073');
INSERT INTO `role_permission` VALUES (2481, '1', '1000000903407910');
INSERT INTO `role_permission` VALUES (2482, '1', '1000000851815490');
INSERT INTO `role_permission` VALUES (2483, '1', '3');
INSERT INTO `role_permission` VALUES (2484, '1', '301');
INSERT INTO `role_permission` VALUES (2485, '1', '4');
INSERT INTO `role_permission` VALUES (2486, '1', '401');
INSERT INTO `role_permission` VALUES (2487, '1', '1000000189866454');
INSERT INTO `role_permission` VALUES (2488, '1', '1000000082934989');
INSERT INTO `role_permission` VALUES (2489, '8', '2');
INSERT INTO `role_permission` VALUES (2490, '8', '201');
INSERT INTO `role_permission` VALUES (2491, '8', '20101');
INSERT INTO `role_permission` VALUES (2492, '8', '20102');
INSERT INTO `role_permission` VALUES (2493, '8', '20103');
INSERT INTO `role_permission` VALUES (2494, '8', '20104');
INSERT INTO `role_permission` VALUES (2495, '8', '20106');
INSERT INTO `role_permission` VALUES (2496, '8', '202');
INSERT INTO `role_permission` VALUES (2497, '8', '20201');
INSERT INTO `role_permission` VALUES (2498, '8', '20202');
INSERT INTO `role_permission` VALUES (2499, '8', '20203');
INSERT INTO `role_permission` VALUES (2500, '8', '20204');
INSERT INTO `role_permission` VALUES (2501, '8', '20205');
INSERT INTO `role_permission` VALUES (2502, '8', '20206');
INSERT INTO `role_permission` VALUES (2503, '8', '203');
INSERT INTO `role_permission` VALUES (2504, '8', '20301');
INSERT INTO `role_permission` VALUES (2505, '8', '20302');
INSERT INTO `role_permission` VALUES (2506, '8', '20303');
INSERT INTO `role_permission` VALUES (2507, '8', '20304');
INSERT INTO `role_permission` VALUES (2508, '8', '1000000884924014');
INSERT INTO `role_permission` VALUES (2509, '8', '1000000433323073');
INSERT INTO `role_permission` VALUES (2510, '8', '1000000903407910');
INSERT INTO `role_permission` VALUES (2511, '8', '1000000851815490');
INSERT INTO `role_permission` VALUES (2535, '9', '2');
INSERT INTO `role_permission` VALUES (2536, '9', '201');
INSERT INTO `role_permission` VALUES (2537, '9', '20101');
INSERT INTO `role_permission` VALUES (2538, '9', '20102');
INSERT INTO `role_permission` VALUES (2539, '9', '20103');
INSERT INTO `role_permission` VALUES (2540, '9', '20104');
INSERT INTO `role_permission` VALUES (2541, '9', '20106');
INSERT INTO `role_permission` VALUES (2542, '9', '202');
INSERT INTO `role_permission` VALUES (2543, '9', '20201');
INSERT INTO `role_permission` VALUES (2544, '9', '20202');
INSERT INTO `role_permission` VALUES (2545, '9', '20203');
INSERT INTO `role_permission` VALUES (2546, '9', '20204');
INSERT INTO `role_permission` VALUES (2547, '9', '20205');
INSERT INTO `role_permission` VALUES (2548, '9', '20206');
INSERT INTO `role_permission` VALUES (2549, '9', '203');
INSERT INTO `role_permission` VALUES (2550, '9', '20301');
INSERT INTO `role_permission` VALUES (2551, '9', '20302');
INSERT INTO `role_permission` VALUES (2552, '9', '20303');
INSERT INTO `role_permission` VALUES (2553, '9', '20304');
INSERT INTO `role_permission` VALUES (2554, '9', '1000000884924014');
INSERT INTO `role_permission` VALUES (2555, '9', '1000000433323073');
INSERT INTO `role_permission` VALUES (2556, '9', '1000000903407910');
INSERT INTO `role_permission` VALUES (2557, '9', '1000000851815490');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加密盐值',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `sex` int(255) NULL DEFAULT NULL COMMENT '年龄：1男2女',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `status` int(1) NOT NULL COMMENT '用户状态：1有效; 2删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`, `user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '1', 'admin', '8bc0b3229f3ebc88299b1e3181f93cb4', '8cd50474d2a3c1e88298e91df8bf6f1c', '123@qq.com', '18953033565', 1, 223, 1, '2018-05-23 21:22:06', '2019-03-02 22:04:37', '2019-01-31 11:16:25');
INSERT INTO `user` VALUES (7, '1000002126123057', 'liyao123', '6fcb44f9b682e7f0b27556a7b7ba3c6a', NULL, '1124863805@qq.com', '18953033565', 1, 199, 1, '2019-03-01 16:05:28', '2019-03-01 16:05:28', '2019-03-01 16:05:28');
INSERT INTO `user` VALUES (8, '1000000595276135', 'liyao1235', 'ee08826c6d715310119485961ed4ab14', NULL, '1124863805@qq.com', '18953033565', 1, 1895, 1, '2019-03-01 16:06:04', '2019-03-01 16:06:04', '2019-03-01 16:06:04');
INSERT INTO `user` VALUES (9, '1000000287094380', 'liyao123566', 'c3f62a4264fda5de8af127b8397ad653', NULL, '123@qq.com', '18953033565', 1, 12, 1, '2019-03-01 16:15:51', '2019-03-01 16:15:51', '2019-03-01 16:15:51');
INSERT INTO `user` VALUES (10, '1000001275423905', 'liyao12r', '0ec600ec632f39a3e7dc7a2711ab9e20', NULL, '1124863805@qq.co', '18953033565', 1, NULL, 1, '2019-03-02 23:31:48', '2019-03-02 23:32:41', '2019-03-02 23:31:48');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `role_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 262 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (256, '1', '1');
INSERT INTO `user_role` VALUES (257, '1', '2');
INSERT INTO `user_role` VALUES (258, '7', '2');
INSERT INTO `user_role` VALUES (259, '10', '1');
INSERT INTO `user_role` VALUES (260, '10', '2');
INSERT INTO `user_role` VALUES (261, '10', '8');

SET FOREIGN_KEY_CHECKS = 1;
