# LShiro

#### 介绍
是一个实现简单的权限后台管理系统，实现在线会话，用户管理，角色管理，资源管理，使用springbooot mybatis shiro redis thymeleaf 
前端使用 layui 当做前端组件，使用INSPINIA后台模板。

#### 软件架构

INSPINIA 模板下载地址 链接：https://pan.baidu.com/s/1fVN36fnqnqbf6iSu5i5lhw 提取码：8n1b 

tab 选项卡原始代码下载地址 链接：https://pan.baidu.com/s/1H__iic4MtnqGBr8eGt3bTw 提取码：oy2j 

![tab选项卡截图](https://images.gitee.com/uploads/images/2019/0303/164023_e463480e_1525155.png "屏幕截图.png")


#### 安装教程

1. 使用idea 导入本项目（elipse没试过）

2. 导入doc文件夹下面的数据库

3. 修改(resources/application.yml)配置文件

4. 直接运行ShiroBootApplication.java （注 不要忘记运行redis哦，没有运行不起来哦）

5. 打开浏览器访问http://localhost:8081

账号：admin 密码：123456


#### 使用说明

项目截图 （没有多余的服务器 没演示环境）
 项目首页
![项目工作台页面](https://images.gitee.com/uploads/images/2019/0303/164318_e09b356b_1525155.png "屏幕截图.png")
用户管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/164910_ebf28d76_1525155.png "屏幕截图.png")
用户管理-编辑角色
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/165024_58c717fe_1525155.png "屏幕截图.png")

角色管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/165117_929b2faa_1525155.png "屏幕截图.png")

角色管理-编辑角色
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/165153_6dacfac0_1525155.png "屏幕截图.png")

资源管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/165254_55065e7f_1525155.png "屏幕截图.png")

资源管理-新增资源
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/165347_555110c5_1525155.png "屏幕截图.png")

在线用户
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/165434_5e076f32_1525155.png "屏幕截图.png")
#### 参与贡献


小小的博客 www.liyao.co     刚刚创的 QQ群 出现问题可以问哦 群号699592120